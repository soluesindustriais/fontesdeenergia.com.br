<?
$h1         = 'Fontes de Energia';
$title      = 'Início - Fontes de Energia';
$desc       = 'Fontes de Energia - Conte com os melhores fornecedores de Energia do Brasil. É gratis!';  
$var        = 'Home';
include('inc/head.php');

?>
</head>
<body>
<? include('inc/topo.php'); ?>
<section class="cd-hero">
  <div class="title-main"> <h1>FONTES DE ENERGIA</h1> </div>
  <ul class="cd-hero-slider autoplay">
    <li class="selected">
      <div class="cd-full-width">
        <h2>GRUPO GERADOR DE ENERGIA</h2>
        <p>A energia elétrica é fundamental na vida de pessoas e empresas. Porém, É crucial saber comprar um gerador de energia para escolher o modelo mais vantajoso e indicado para cada necessidade.</p>
        <a href="<?=$url?>grupo-gerador-de-energia" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>PLACA DE ENERGIA SOLAR</h2>
        <p>A placa de energia solar geralmente é consumida em módulos que tem largura fixa de 1,20 metros e comprimentos variados. O produto pode ser de 2,00 ou 3,00 metros, se adaptando ao que é solicitado.</p>
        <a href="<?=$url?>placa-de-energia-solar" class="cd-btn">Saiba mais</a>
      </div>
    </li>
    <li>
      <div class="cd-full-width">
        <h2>TRANSFORMADOR DE ENERGIA</h2>
        <p>Os transformadores de energia são fundamentais para as pessoas e empresas, pois são produtos responsáveis por garantir uma excelente qualidade no uso de energia elétrica em indústrias ou residências.</p>
        <a href="<?=$url?>transformador-de-energia" class="cd-btn">Saiba mais</a>
      </div>
    </li>
  </ul>
  <div class="cd-slider-nav">
    <nav>
      <span class="cd-marker item-1"></span>
      <ul>
        <li class="selected"><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
        <li><a href="#0"><i class="fa fa-circle" aria-hidden="true"></i></a></li>
      </ul>
    </nav>
  </div>
</section>
<main>
  <section class="wrapper-main">
    <div class="main-center">
      <div class=" quadro-2 ">
        <h2>ENERGIA SOLAR</h2>
        <div class="div-img">
          <p data-anime="left-0">Toda empresa necessita de um planejamento adequado para prevenir possíveis interrupções e imprevistos, por essa razão é sempre importante contar com uma equipe especializada em solucionar problemas, assim esta tarefa se torna mais corriqueira e menos impactante na linha de produção industrial, esse processo é chamado de manutenção preventiva.</p>
        </div>
        <div class="gerador-svg" data-anime="in">
          <img src="imagens/img-home/aluguel-empilhadeira.jpg" alt="Aluguel Empilhadeira" title="Aluguel Empilhadeira">
        </div>
      </div>
      <div class=" incomplete-box">
        <ul data-anime="in">
          <li>
            <p>No caso da manutenção preventiva torre de resfriamento, por exemplo, a manutenção preventiva auxilia em uma série de fatores, como por exemplo:</p>
            <li><i class="fa fa-angle-right"></i>Reduzir riscos de quebra;</li>
            <li><i class="fa fa-angle-right"></i>Prevenir o envelhecimento e degeneração dos equipamentos;</li>
            <li><i class="fa fa-angle-right"></i>Programar a conversação das peças;</li>
            <li><i class="fa fa-angle-right"></i>Amenizar os custos de compra de novos itens.</li>
          </ul>
          <a href="<?=$url?>placa-de-energia-solar" class="btn-4">Saiba mais</a>
        </div>
      </div>
      <div id="content-icons">
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fa fa-bolt fa-7x"></i>
            <div>
              <p>TUDO SOBRE FORNECEDORES DE ENERGIA</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fa fa-calendar fa-7x"></i>
            <div>
              <p>GERADORES DE ENERGIA</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fa fa-building fa-7x"></i>
            <div>
              <p>COTE COM DIVERSAS EMPRESAS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fa fa-dollar fa-7x"></i>
            <div>
              <p>COMPARE PREÇOS</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
            <i class="fa fa-envira fa-7x"></i>
            <div>
              <p>ENERGIA SUSTENTÁVEL</p>
            </div>
          </div>
        </div>
        <div class="co-icon">
          <div class="quadro-icons" data-anime="in">
          <i class="fa fa-handshake-o fa-7x"></i>
            <div>
              <p>FAÇA O MELHOR NEGÓCIO</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wrapper-img">
      <div class="txtcenter">
        <h2>Produtos <b>Relacionados</b></h2>
      </div>
       <div class="content-icons">

        <div class="produtos-relacionados-1">
          <figure>
            <div class="fig-img2">
            <a href="<?=$url?>placa-de-energia-solar">
              <h2>PLACA DE ENERGIA SOLAR</h2>
                </a>
                <a href="<?=$url?>placa-de-energia-solar">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
              </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-2">
          <figure class="figure2">
              <div class="fig-img2">
            <a href="<?=$url?>transformador-de-energia">
                <h2>TRANSFORMADOR DE ENERGIA</h2>
            </a>
                <a href="<?=$url?>transformador-de-energia">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>

        <div class="produtos-relacionados-3">
          <figure>
              <div class="fig-img2">
              <a href="<?=$url?>gerador-de-energia-a-diesel">
                <h2>GERADOR DE ENERGIA A DIESEL</h2>
                </a>
                <a href="<?=$url?>gerador-de-energia-a-diesel">
                <span class="btn-5" data-anime="up"> Saiba Mais </span>
                </a>
              </div>
          </figure>
        </div>
      </div>
    </section>
    <section class="wrapper-destaque">
      <div class="destaque txtcenter">
        <h2>Galeria de <b>Produtos</b></h2>
        <div class="center-block txtcenter">
          <ul class="gallery">
            <li><a href="<?=$url?>imagens/img-home/galeria-eficiencia-energetica.jpg" class="lightbox" title="Eficiência energética">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-eficiencia-energetica.jpg" title="Eficiência energética" alt="Eficiência energética">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-auto-transformador.jpg" class="lightbox"  title="Auto transformador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-auto-transformador.jpg" alt="Auto transformador" title="Auto transformador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-comprar-grupo-gerador.jpg" class="lightbox" title="Comprar grupo gerador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-comprar-grupo-gerador.jpg" alt="Comprar grupo gerador" title="Comprar grupo gerador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-compra-de-gerador-de-energia.jpg" class="lightbox" title="Compra de gerador de energia">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-compra-de-gerador-de-energia.jpg" alt="Compra de gerador de energia" title="Compra de gerador de energia">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-comprar-placa-solar.jpg" class="lightbox" title="Comprar placa solar">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-comprar-placa-solar.jpg" alt="Comprar placa solar"  title="Comprar placa solar">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-grupos-geradores-a-diesel.jpg" class="lightbox" title="Grupos geradores a diesel">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-grupos-geradores-a-diesel.jpg" alt="Grupos geradores a diesel" title="Grupos geradores a diesel">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empresa-fabricante-de-transformadores.jpg" class="lightbox" title="Empresa fabricante de transformadores">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empresa-fabricante-de-transformadores.jpg" alt="Empresa fabricante de transformadores" title="Empresa fabricante de transformadores">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empresa-de-eficiencia-energetica.jpg" class="lightbox" title="Empresa de eficiência energética">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empresa-de-eficiencia-energetica.jpg" alt="Empresa de eficiência energética" title="Empresa de eficiência energética">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-preco-de-transformador.jpg" class="lightbox" title="Preço de transformador">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-preco-de-transformador.jpg" alt="Preço de transformador" title="Preço de transformador">
              </a>
              </li>
              <li><a href="<?=$url?>imagens/img-home/galeria-empresa-de-eletrica-industrial.jpg" class="lightbox" title="Empresa de elétrica industrial">
              <img src="<?=$url?>imagens/img-home/thumbs/galeria-empresa-de-eletrica-industrial.jpg" alt="Empresa de elétrica industrial" title="Empresa de elétrica industrial">
              </a>
              </li>
          </ul>
        </div>
      </div>
    </section>
    <div class="wrapper">
      <h2>Produtos mais vendidos</h2>
      <ul class="thumbnails-mod1">
                    <li>
                      <a href="<?=$url?>placa-de-energia-solar" title="Placa de energia solar"><img src="<?=$url?>imagens/mpi/thumbs/placa-de-energia-solar-preco-01.jpg" alt="Placa de energia solar" title="Placa de energia solar"></a>
                      <h2><a href="<?=$url?>placa-de-energia-solar" title="Placa de energia solar">Placa de energia solar</a></h2>
                    </li>
                    <li>
                      <a href="<?=$url?>placa-de-energia-solar-preco" title="Placa de energia solar preço"><img src="<?=$url?>imagens/mpi/thumbs/placa-de-energia-solar-preco-02.jpg" alt="Placa de energia solar preço" title="Placa de energia solar preço"></a>
                      <h2><a href="<?=$url?>placa-de-energia-solar-preco" title="Placa de energia solar preço">Placa de energia solar preço</a></h2>
                    </li>
                    <li>
                      <a href="<?=$url?>empresa-de-placas-solares" title="Empresa de placas solares"><img src="<?=$url?>imagens/mpi/thumbs/placa-de-energia-solar-preco-03.jpg" alt="Empresa de placas solares" title="Empresa de placas solares"></a>
                      <h2><a href="<?=$url?>empresa-de-placas-solares" title="Empresa de placas solares">Empresa de placas solares</a></h2>
                    </li>
                    <li>
                      <a href="<?=$url?>placa-de-energia-solar-industrial" title="Placa de energia solar industrial"><img src="<?=$url?>imagens/mpi/thumbs/placa-de-energia-solar-02.jpg" alt="Placa de energia solar industrial" title="Placa de energia solar industrial"></a>
                      <h2><a href="<?=$url?>placa-de-energia-solar-industrial" title="Placa de energia solar industrial">Placa de energia solar industrial</a></h2>
                    </li>
                  </ul>
      
      
                           
                            
      
    </div>
</main>
<? include('inc/footer.php'); ?>
<script src="<?=$url?>hero/js/modernizr.js"></script>
<script src="<?=$url?>hero/js/main.js"></script>
</body>
</html>