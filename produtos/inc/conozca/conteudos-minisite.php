<?php 
$conteudo1 = [
"<h2>Painel Automático para Gerador</h2>
<p>O <strong>painel automático para gerador</strong> é um dispositivo essencial para garantir o funcionamento eficiente e seguro dos geradores de energia. Este painel monitora constantemente as condições de energia e aciona automaticamente o gerador em caso de falha na rede elétrica. Além disso, ele controla e gerencia a operação do gerador, otimizando seu desempenho e garantindo uma alimentação contínua de energia para seus equipamentos críticos.</p>"
];
$conteudo2 = [
"<h2>Painel de Transferência Automática</h2>
<p>O <strong>painel de transferência automática</strong> é um componente vital para sistemas de energia de emergência. Ele detecta a perda de energia da rede e automaticamente transfere a carga elétrica para o gerador, garantindo que a energia seja restaurada rapidamente. Esse painel é amplamente utilizado em hospitais, data centers e outras instalações onde a continuidade da energia é crucial.</p>"
];

$conteudo3 = [
"<h2>Painel de Transferência Automática para Geradores</h2>
<p>O <strong>painel de transferência automática para geradores</strong> é projetado para integrar perfeitamente com geradores, facilitando a transição de energia em caso de falhas na rede elétrica. Este painel oferece monitoramento e controle avançados, assegurando que o gerador inicie e assuma a carga sem interrupções significativas. É uma solução confiável para manter operações críticas em funcionamento contínuo.</p>"
];

$conteudo4 = [
"<h2>Quadro de Transferência Automática para Gerador</h2>
<p>O <strong>quadro de transferência automática para gerador</strong> atua como um intermediário entre a rede elétrica e o gerador, garantindo que a transferência de energia ocorra de maneira automática e eficiente. Este quadro é fundamental para sistemas de backup, proporcionando segurança e confiabilidade ao alternar entre as fontes de energia sem intervenção manual.</p>"
];

$conteudo5 = [
"<h2>Gerador Pequeno a Diesel</h2>
<p>O <strong>gerador pequeno a diesel</strong> é uma opção prática e eficiente para fornecer energia em locais onde a conexão à rede elétrica é limitada ou inexistente. Estes geradores são compactos, fáceis de transportar e oferecem uma solução de energia confiável para pequenas aplicações comerciais e residenciais. Eles são ideais para uso em obras, eventos e como fonte de energia de emergência.</p>"
];

$conteudo6 = [
"<h2>Gerador de Energia Automático</h2>
<p>O <strong>gerador de energia automático</strong> é uma solução avançada que inicia e opera automaticamente em resposta a falhas de energia. Equipado com tecnologia de monitoramento e controle, este tipo de gerador garante a continuidade da energia sem a necessidade de intervenção manual. É perfeito para instalações críticas onde a interrupção de energia não é uma opção.</p>"
];

$conteudo7 = [
"<h2>Gerador Trifásico 15kVA</h2>
<p>O <strong>gerador trifásico 15kVA</strong> é uma escolha robusta para aplicações que requerem uma maior capacidade de energia. Este gerador é capaz de suportar cargas significativas e é ideal para uso em indústrias, construções e grandes eventos. A sua configuração trifásica proporciona um fornecimento de energia mais estável e eficiente para equipamentos de alta demanda.</p>"
];

$conteudo8 = [
"<h2>Quadro de Transferência Automática</h2>
<p>O <strong>quadro de transferência automática</strong> é crucial para a operação segura e eficiente de sistemas de energia de backup. Este dispositivo permite a troca automática entre a rede elétrica e o gerador, garantindo que a energia seja restabelecida rapidamente durante quedas de energia. Sua instalação é essencial para qualquer sistema que exija continuidade ininterrupta de energia.</p>"
];

$conteudo9 = [
"<h2>Manutenção Preventiva de Geradores de Energia</h2>
<p>A <strong>manutenção preventiva de geradores de energia</strong> é fundamental para assegurar o funcionamento eficiente e a longevidade dos geradores. Esta prática inclui inspeções regulares, testes e substituição de componentes desgastados antes que falhem. A manutenção preventiva ajuda a evitar paradas inesperadas e garante que os geradores estejam sempre prontos para operar em caso de emergências.</p>
"
];

$conteudo10 = [
"<h2>Manutenção de Geradores de Energia</h2>
<p>A <strong>manutenção de geradores de energia</strong> envolve uma série de procedimentos técnicos para garantir que os geradores funcionem de maneira eficiente e confiável. Este serviço abrange desde a verificação de fluidos e filtros até a inspeção de sistemas elétricos e mecânicos. A manutenção regular é essencial para prevenir falhas, prolongar a vida útil do equipamento e garantir a disponibilidade de energia quando necessário.</p>"
];

$metadescription = "Oferecemos soluções completas em sistemas de resfriamento, umidificação, desinfecção e abatimento de poeira, com equipamentos de alta qualidade e eficiência para diversas aplicações industriais e comerciais.";

?>