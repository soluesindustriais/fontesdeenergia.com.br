<section class="wrapper sobre-product article">
            <div class="swiper wrapper">
                <div class="swiper-wrapper"> 
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <div class="slides-divs">
                            <h3><?=$product;?></h3>
                            <img src="imagens/imgvertical.jpg" alt="img-vertical">
                            <h3><?=$product_subtitle;?></h3>
                            <p><?=$product_description;?></p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slides-divs">
                            <h3><?=$product2;?></h3>
                            <img src="imagens/imgvertical.jpg" alt="img-vertical">
                            <h3><?=$product_subtitle2;?></h3>
                            <p><?=$product_description2;?></p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="slides-divs">
                            <h3><?=$product3;?></h3>
                            <img src="imagens/imgvertical.jpg" alt="img-vertical">
                            <h3><?=$product_subtitle3;?></h3>
                            <p><?=$product_description3;?></p>
                        </div>
                    </div>
                </div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </section>
