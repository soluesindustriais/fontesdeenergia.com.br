<? $h1 = "Contato";
$title  =  "Entre em Contato";
$desc = "Compare godoi, você vai encontrar na plataforma Soluções Industriais, receba uma estimativa de valor pela internet com aproximadamen";
$key  = "Entre em Contato";
include 'inc/head.php';

?>
<style>
    <?
    include('css/header-script.css');
    include "$linkminisite" . "css/style.css";
    include "$linkminisite" . "css/mpi.css";
    include "$linkminisite" . "css/normalize.css";
    include "$linkminisite" . "css/aside.css";
    ?>
</style>
</head>

<? include "inc/header-dinamic.php" ?>


<?= $caminho ?>
<main class="content-index m-20">
    <section class="wrapper article d-flex justify-content-between align-items-stretch flex-wrap-mobile">

        <!-- Contêineres de formulários dinâmicos -->
        <div data-sdk-form-builder-solucoes data-form-id="23" class="form-soluc"></div>
        <!-- Adicione os scripts do SDK e implementação aqui... -->


        <aside class="aside d-flex justify-content-start align-items-stretch flex-column aside-contact">
            <div class="aside-contact-info">
                <div class="i-form">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <p>Endereço</p>
                </div>

                <p><?= $rua ?></p>
            </div>
            
            <div class="aside-contact-info">
                <div class="i-form">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>Telefone</p>
                </div>

                <p>(11) 2093-8108</p>
            </div>
            <!-- <div class="aside-contact-info">
                <div class="i-form">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <p>Siga-nos</p>
                </div>
                <p>
                    <a href="#" class="social-icons"><i class="fa-brands fa-instagram"></i></a>
                    <a href="#" class="social-icons"><i class="fa-brands fa-facebook-f"></i></a>
                </p>
            </div> -->
        </aside>
    </section>

</main>
<? include('inc/footer.php'); ?>
<script>
    $(document).ready(function() {
        // Inicialize o FormRendererSDK
        FormRendererSDK.initialize();
        // Registre callbacks para eventos, se necessário
        FormRendererSDK.on('formRendered', function(container) {
            // Lógica após a renderização bem-sucedida do formulário
            $("input[type='tel']").mask("(00) 00000-0000");
        });
    });
</script>

<style>
    .form-soluc {
  width: 100%;
  margin: 15px;

  & form {
    display: flex;
    flex-direction: column;
    gap: 20px;
    border: 1px solid #ccc;
    padding: 30px;
    border-radius: 8px;
  }

  .btn {
    display: flex !important;
    width: 100%;
    justify-content: center;
    align-items: center;
    height: 50px;
    font-size: 1rem;
    width: 300;
    background-color: var(--cor-principaldocliente);
    border: 0px !important;
    color: #fff;
  }
}

.form-group {
  display: flex;
  flex-direction: column;
  gap: 8px;

  & label {
    display: flex;
    gap: 8px;
    font-weight: 600;
    color: var(--cor-principaldocliente)
  }
  & input {
    width: 100%;
    border-radius: 3px;
    height: 40px;
    padding: 10px;
  }
  & textarea {
    width: 100%;
    resize: none;
    border: 1px solid #ccc !important;
    border-radius: 3px;
    padding: 10px;
  }
}
</style>