<?php
$clienteAtivo = "ativo";

//HOMEsolucs
// $tituloCliente recebe ($clienteAtivo igual 'inativo') recebe 'Soluções industriais' se não 'Base Mini Site';
$tituloCliente = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Conozca';
$tituloCliente = str_replace(' ', '-', $tituloCliente);

$subTituloCliente = 'GRUPOS GERADORES';
$bannerIndex = 'imagem-banner-fixo';
$minisite = "inc/" . $tituloCliente . "/";
$subdominio = str_replace(' ', '-', $tituloCliente);




//informacoes-vetPalavras
$CategoriaNameInformacoes = "informacoes";


include("$linkminisite" . "conteudos-minisite.php");


// Criar página de produto mpi
$VetPalavrasProdutos = [
    "painel-automatico-para-gerador",
    "painel-de-transferencia-automatica",
    "painel-de-transferencia-automatica-para-geradores",
    "quadro-de-transferencia-automatica-para-gerador",
    "gerador-pequeno-a-diesel",
    "gerador-de-energia-automatico",
    "gerador-trifasico-15kva",
    "quadro-de-transferencia-automatica"
];

//Criar página de Serviço
$VetPalavrasInformacoes = [
    "manutencao-preventiva-de-geradores-de-energia",
    "manutencao-de-geradores-de-energia",
];



// Numero do formulario de cotação
$formCotar = 225;



// Informações Geral.php

$nomeSite = ($clienteAtivo == 'inativo') ? 'Soluções industriais' : 'Conozca';
$slogan = ($clienteAtivo == 'inativo') ? "Inovando sua indústria com eficiência" : 'GRUPOS GERADORES';
$rua = ($clienteAtivo == 'inativo') ? "Rua Alexandre Dumas" : 'Av. Severino Josino Guerra';
$bairro = ($clienteAtivo == 'inativo') ? "Santo Amaro" : 'Paratibe';
$cidade = ($clienteAtivo == 'inativo') ? "São Paulo" : 'Paulista';
$UF = ($clienteAtivo == 'inativo') ? "SP" : 'PE';
$cep = ($clienteAtivo == 'inativo') ? "CEP: 04717-004" : '53413-195';
$imgLogo = ($clienteAtivo == 'inativo') ? 'logo-site.webp' : 'logo-cliente-fixo.svg';
$emailContato = ($clienteAtivo == 'inativo') ? '' : 'contato@conozca.com.br';
$instagram =  ($clienteAtivo == 'inativo') ? 'https://www.instagram.com/solucoesindustriaisoficial?igsh=MWtvOGF4aXBqODF3MQ==' : '';
$facebook =  ($clienteAtivo == 'inativo') ? 'https://www.facebook.com/solucoesindustriais/' : '';



?>


<!-- VARIAVEIS DE CORES -->
<style>
    :root {
        --cor-pretopantone: #2d2d2f;
        --cor-principaldocliente: #003060;
        --cor-secundariadocliente: #4876b1;
        --terceira-cor: #2d2d2f;
        --font-primary: "Poppins";
        --font-secundary: "Poppins";
    }
</style>