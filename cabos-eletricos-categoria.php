<? $h1 = "Cabos elétricos - Categoria";
$title  = "Cabos elétricos - Categoria";
$desc = "Encontre $h1, encontre as melhores indústrias, receba diversos comparativos imediatamente com mais de 200 empresas de todo o Brasil";
$key  = "";
include('inc/head.php'); ?>
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<script>
    $(function() {
        $(' .thumbnails > li ').each(function() {
            $(this).hoverdir({
                hoverDelay: 75
            });
        });
    });
</script>
</head>

<body> <? include('inc/topo.php'); ?> <div class="wrapper">
        <main role="main">
            <div class="content">
                <section> <?= $caminho ?> <?php include_once('inc/cabos-eletricos/cabos-eletricos-buscas-relacionadas.php'); ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article class="full">
                        <p>O mercado de <?= $h1 ?> é amplo e conta com produtos e serviços que podem ser úteis em diversas aplicações. No Soluções Industriais, portal especializado na geração de negócios para o mercado B2B, é possível encontrar as melhores empresas que atuam nesse segmento.</p>
                        <p>Além de receber um orçamento, você também poderá esclarecer suas dúvidas referentes ao assunto. Saiba mais sobre <?= $h1 ?> e faça uma cotação.</p>
                        <ul class="thumbnails-2"> <?php include_once('inc/cabos-eletricos/cabos-eletricos-categoria.php'); ?> </ul>
                    </article> <br class="clear">
                </section>
            </div>
        </main>
    </div><!-- .wrapper --> <? include('inc/footer.php'); ?> </body>

</html>