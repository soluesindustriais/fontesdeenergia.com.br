import re

# Define os caminhos dos arquivos
arquivo_original = 'style.css'  # Substitua pelo caminho do seu arquivo CSS
arquivo_com_media = 'media_queries.css'
arquivo_sem_media = 'estilo_sem_media.css'

# Lê o conteúdo do arquivo CSS original
with open(arquivo_original, 'r', encoding='utf-8') as file:
    css_content = file.read()

# Define a expressão regular para encontrar as media queries
media_query_pattern = re.compile(r'(@media[^{]*\{[\s\S]*?\}\s*\})', re.MULTILINE)

# Encontra e remove as media queries do conteúdo original
media_queries = media_query_pattern.findall(css_content)
css_content_sem_media = media_query_pattern.sub('', css_content)

# Salva as media queries em um novo arquivo, se existirem
if media_queries:
    with open(arquivo_com_media, 'w', encoding='utf-8') as mq_file:
        for query in media_queries:
            mq_file.write(query + "\n")
    print(f"As media queries foram extraídas e salvas em {arquivo_com_media}.")

# Salva o CSS sem as media queries em outro arquivo
with open(arquivo_sem_media, 'w', encoding='utf-8') as no_mq_file:
    no_mq_file.write(css_content_sem_media)
print(f"O CSS sem as media queries foi salvo em {arquivo_sem_media}.")
