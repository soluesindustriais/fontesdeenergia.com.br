<? $h1 = "Preço do transformador de energia";
$title  = "Preço do transformador de energia";
$desc = "Se procura por $h1, veja os melhores distribuidores, realize um orçamento pelo formulário com mais de 30 fábricas ao mesmo tempo";
$key  = "Transformador de energia,Valor do transformador de energia";
include('inc/head.php');  ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/preco-do-transformador-de-energia-01.jpg" title="<?= $h1 ?>" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/preco-do-transformador-de-energia-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/preco-do-transformador-de-energia-02.jpg" title="Transformador de energia" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/preco-do-transformador-de-energia-02.jpg" title="Transformador de energia" alt="Transformador de energia"></a><a href="<?= $url ?>imagens/mpi/preco-do-transformador-de-energia-03.jpg" title="Valor do transformador de energia" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/preco-do-transformador-de-energia-03.jpg" title="Valor do transformador de energia" alt="Valor do transformador de energia"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>O transformador de energia é um equipamento capaz de transformar aparelhos 220V em aparelhos 380V (ou vice-versa) em questão de poucos segundos. A utilização da aparelhagem se torna possível de modo seguro e sem qualquer tipo de obra ou alteração na instalação elétrica do ambiente em questão.</p>
                        <p>A frequência do transformador de energia varia (em média, 60hz), a tensão é de 440 / 380V / 220V e a potência máxima varia conforme as necessidades – ou seja, com base no produto que será ligado no transformador de energia.</p>
                        <h2>Principais benefícios</h2>
                        <p>O transformador de energia é um equipamento muito potente onde pode ser aplicado em ambientes com os seguintes níveis de tensão:</p>
                        <ul>
                            <li class="li-mpi">440V;</li>
                            <li class="li-mpi">380V;</li>
                            <li class="li-mpi">220V.</li>
                        </ul>
                        <p>Você pode se interessar também por <a target='_blank' title='Transformador de energia' href="<?= $url?>transformador-de-energia">Transformador de energia</a>. Veja mais detalhes ou solicite um <b>orçamento gratuito</b> com um dos fornecedores disponíveis!</p>
                        <h2>Escolha do fornecedor</h2>
                        <p> Ao buscar pelo melhor <strong>preço do transformador de energia</strong> basta clicar no botão indicado e solicitar o orçamento com as empresas.</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>