<? $h1 = "Fonte de alimentação ac";
$title  = "Fonte de alimentação ac";
$desc = "Compare preços de $h1, você consegue nos resultados do Soluções Industriais, orce hoje mesmo com mais de 50 distribuidores";
$key  = "Fontes de alimentação ac,Comprar fonte de alimentação ac";
include('inc/head.php');  ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminhoinformacoes ?><br class="clear" />
                    <h1><?= $h1 ?></h1>
                    <article>
                        <div class="img-mpi"><a href="<?= $url ?>imagens/mpi/fonte-de-alimentacao-ac-01.jpg" title="<?= $h1 ?>" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/fonte-de-alimentacao-ac-01.jpg" title="<?= $h1 ?>" alt="<?= $h1 ?>"></a><a href="<?= $url ?>imagens/mpi/fonte-de-alimentacao-ac-02.jpg" title="Fontes de alimentação ac" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/fonte-de-alimentacao-ac-02.jpg" title="Fontes de alimentação ac" alt="Fontes de alimentação ac"></a><a href="<?= $url ?>imagens/mpi/fonte-de-alimentacao-ac-03.jpg" title="Comprar fonte de alimentação ac" class="lightbox"><img src="<?= $url ?>imagens/mpi/thumbs/fonte-de-alimentacao-ac-03.jpg" title="Comprar fonte de alimentação ac" alt="Comprar fonte de alimentação ac"></a></div><span class="aviso">Estas imagens foram obtidas de bancos de imagens públicas e disponível livremente na internet</span>
                        <hr />
                        <p>Constituída por várias fontes de potências que são capazes de comutação de alta potência AC, DC e AC + DC, a <strong>fonte de alimentação AC</strong> série AFX da Pacific Power éconsidera uma das melhores opções para várias aplicações. Todos os modelos oferecem configurações de saída simples, divididas e trifásicas.</p>
                        <h2>Mais características da fonte de alimentação AC</h2>
                        <p>A fonte de alimentação modelo AC é um projeto de última geração que utiliza as mais recentes tecnologias de conversão de energia digital. A densidade de potência está entre as mais altas da indústria, permitindo 15 kVA / kW de potência em apenas 4U (7 “ ) de espaço no rack. Além disso oferece:</p>
                        <ul>
                            <li class="li-mpi">Eficiência energética;</li>
                            <li class="li-mpi">Facilidade de instalação;</li>
                            <li class="li-mpi">Potência máxima por polegada cúbica de espaço no rack.</li>
                        </ul>
                        <p>Você pode se interessar também por <a target='_blank' title='Transformador de energia' href="<?= $url?>transformador-de-energia">Transformador de energia</a>. Veja mais detalhes ou solicite um <b>orçamento gratuito</b> com um dos fornecedores disponíveis!</p>
                        <p>Além disso, as funções de controle e operacional fornecem um alto grau de versatilidade de aplicação e facilidade de uso para o engenheiro de teste. As aplicações variam desde conversão de frequência simples, controlada manualmente até testes de imunidade harmônica e simulação transitória sofisticada.</p>
                        <h2>Empresa especializada em fontes de alimentação</h2>
                        <p>Entre em contato com a equipe técnica da Ohmini e conheça as séries de fonte de alimentação AC, DC e AC+DC e adquira o modelo ideal.</p>
                        <p>Veja também <a href="https://www.fontesdeenergia.com.br/placa-de-energia-solar" style="cursor: pointer; color: #006fe6;font-weight:bold;">Placa de Energia Solar</a>, e solicite agora mesmo uma <b>cotação gratuita</b> com um dos fornecedores disponíveis!</p>
                    </article><? include('inc/coluna-mpi.php'); ?><br class="clear"><? include('inc/busca-mpi.php'); ?><? include('inc/form-mpi.php'); ?><? include('inc/regioes.php'); ?>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>