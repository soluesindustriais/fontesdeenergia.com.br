<?
$h1         = 'Produtos';
$title      = 'Produtos';
$desc       = 'Encontre diversos equipamentos e peças automotivas das melhores empresas. Receba diversos comparativos pelo formulário com mais de 200 fornecedores. É grátis!';
$key        = 'produtos, materiais, equipamentos';
$var        = 'produtos';
include('inc/head.php');
?>
</head>
<body>

<? include('inc/topo.php');?>
<div class="wrapper">
 <main>
    <div class="content">
      <?=$caminho2?>
      <h1>Produtos</h1>   
      <article class="full">   
        <p>Encontre diversos produtos de aço das melhores empresas, para suas necessidades. Receba diversos comparativos pelo formulário com mais de 200 fornecedores.</p>
        <ul class="thumbnails-main">

          <li>
            <a rel="nofollow" href="<?=$url?>cabos-eletricos-categoria" title="Cabos elétricos"><img src="imagens/produtos/cabos-eletricos-01.jpg" alt="Cabos elétricos" title="Cabos elétricos"/></a>
            <h2><a href="<?=$url?>cabos-eletricos-categoria" title="Cabos elétricos">Cabos elétricos</a></h2>
          </li>

          <li>
            <a rel="nofollow" href="<?=$url?>fonte-de-energia-categoria" title="Fonte de energia"><img src="imagens/produtos/fonte-de-energia-01.jpg" alt="Fonte de energia" title="Fonte de energia"/></a>
            <h2><a href="<?=$url?>fonte-de-energia-categoria" title="Fonte de energia ">Fonte de energia  </a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>nobreak-categoria" title="Nobreak"><img src="imagens/produtos/nobreak-01.jpg" alt="Nobreak" title="Nobreak"/></a>
            <h2><a href="<?=$url?>nobreak-categoria" title="Nobreak">Nobreak</a></h2>
          </li>
          <li>
            <a rel="nofollow" href="<?=$url?>transformador-categoria" title="Transformador"><img src="imagens/produtos/transformador-01.jpg" alt="Transformador" title="Transformador"/></a>
            <h2><a href="<?=$url?>transformador-categoria" title="Transformador">Transformador</a></h2>
          </li>

        </ul>
      </article>
    </div>
  </main>
  <? include('inc/form-mpi.php');?>
</div>
<? include('inc/footer.php');?>

</body>
</html>