
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Analisador de energia','Analisador de energia dranetz','Analisador de energia fluke','Analisador de energia fotovoltaica','Analisador de harmônicas','Analisador de potência','Analisador de potência de precisão','Auto trafo trifásico','Banco de carga rlc','Carga eletrônica ac','Carga eletrônica ca','Carga eletrônica cc','Carga eletrônica dc','Carga eletrônica programável','Carga eletrônica regenerativa','Chicote elétrico para residências','Chicote elétrico residencial','Chicotes elétricos','Comprar estabilizador','Controlador de fluxo de massa','Conversor de frequência','Estabilizador 220v','Estabilizador bivolt','Estabilizador de energia','Estabilizador preço','Fabricante de plugues injetados','Fonte simuladora fotovoltaica','Fonte simuladora programável','Fornecedores de plugues injetados','Galvanostato nhr','Medidor baratron','Medidor cátodo frio quente','Medidor de alto vácuo','Medidor de pressão de vácuo','Medidor de vácuo capacitivo','Medidor mavowatt','Medidor pirani','Plug injetado com rabicho','Plugues injetados','Potenciostato nhr','Qualímetro portátil','Rabicho injetado','Simulador de bateria bidirecional','Simulador de rede elétrica','Venda de carregador de bateria','Wattímetro de precisão','Wattímetro oscilógrafo'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "fonte-de-energia";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/fonte-de-energia/fonte-de-energia-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/fonte-de-energia/fonte-de-energia-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/fonte-de-energia/thumbs/fonte-de-energia-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>