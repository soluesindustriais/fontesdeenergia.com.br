<!-- START API -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
<!-- END API -->

<aside class="aside-mpi">
    <div style="display:flex;flex-wrap:wrap;flex-direction:column;">

        <!-- <script>
            pluginOrcamentoJs.init({
            industria: 'solucoes-industriais',
            btnOrcamento: '#btnOrcamento',
            titulo: 'h1'
            });
        </script> -->
        <!-- <p>Cote por <br><?= $h1 ?></p> -->
        <div class="float-banner2" id="float-banner">
            <a id="btnOrcamento" class="btn-produto botao-cotar"><i class="fa fa-envelope"></i>
                Solicite um Orçamento
            </a>
        </div>
    </div>
</aside>

<script>
    $(document).ready(function() {
        $(window).scroll(function() {
            var posicaoAtual = $(window).scrollTop();
            var documentSize = $(document).height();
            var sizeWindow = $(window).height();
            var rodape = $("#rodape").height() + $("#rodape2").height() + 30;

            if (posicaoAtual >= 400) {
                $("#float-banner").removeClass('float-banner2');
                $("#float-banner").addClass('float-banner');
                $("#float-banner").css("left", parseInt(screen.width - 300) + "px");
                if (posicaoAtual >= ((documentSize - sizeWindow) - rodape)) {
                    $("#float-banner").css("bottom", parseInt(posicaoAtual - ((documentSize - sizeWindow) - rodape)) + "px");
                } else {
                    $("#float-banner").css("bottom", "0px");
                }

            } else {
                $("#float-banner").removeClass('float-banner');
                $("#float-banner").addClass('float-banner2');
                $("#float-banner").css("left", "0px");
            }
        });

    });
</script>