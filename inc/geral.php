<?
$nomeSite			= 'Fontes de Energia';
$slogan				= 'Conte com os melhores fornecedores de Energia do Brasil';

$dir = $_SERVER['SCRIPT_NAME'];
$dir = pathinfo($dir);
$host = $_SERVER['HTTP_HOST'];
$http = $_SERVER['REQUEST_SCHEME'];
if ($dir["dirname"] == "/") { $url = $http."://".$host."/"; }
else { $url = $http."://".$host.$dir["dirname"]."/";  }


$emailContato		= 'everton.lima@solucoesindustriais.com.br';
$rua				= 'Rua Pequetita, 179';
$bairro				= 'Vila Olimpia';
$cidade				= 'São Paulo';
$UF					= 'SP';
$cep				= 'CEP: 04552-060';
$latitude			= '-22.546052';
$longitude			= '-48.635514';
$idAnalytics		= 'UA-121697319-1';
$senhaEmailEnvia	= '102030'; // colocar senha do e-mail mkt@dominiodocliente.com.br
$explode			= explode("/", $_SERVER['PHP_SELF']);
$urlPagina 			= end($explode);
$urlPagina	 		= str_replace('.php','',$urlPagina);
$urlPagina 			== "index"? $urlPagina= "" : "";
$urlAnalytics = str_replace("http://www.", '', $url);
$urlAnalytics = str_replace("/", '', $urlAnalytics);
$tagsanalytic = ["G-LKB071BBGT","G-D23WW3S4NC"];
//reCaptcha do Google
$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
//Breadcrumbs

$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]); 

$caminho2 ='
	<div class="breadcrumb" id="breadcrumb" style="margin-top: 1  0px;">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home   </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="2" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	  ';

$caminho ='
	<div class="breadcrumb" id="breadcrumb">
		<div class="wrapper">
			<div class="bread__row">
	<nav aria-label="breadcrumb">
	  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <a href="'.$url.'" itemprop="item" title="Home">
			<span itemprop="name"> Home   </span>
		  </a>
		  <meta itemprop="position" content="1" />
		</li>   
		<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
		<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
		  <span itemprop="name">'.$h1.'</span>
		  <meta itemprop="position" content="3" />
		</li>
	  </ol>
	</nav>
	</div>
	</div>
	</div>
	';

$caminhocabos_eletricos='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'cabos-eletricos-categoria" itemprop="item" title="Cabos-eletricos - Categoria">
                      <span itemprop="name">Cabos eletricos - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhofonte_de_energia='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'fonte-de-energia-categoria" itemprop="item" title="Fonte-de-energia - Categoria">
                      <span itemprop="name">Fonte de energia - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhomateriais_eletricos='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'materiais-eletricos-categoria" itemprop="item" title="Materiais-eletricos - Categoria">
                      <span itemprop="name">Materiais eletricos - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhonobreak='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'nobreak-categoria" itemprop="item" title="Nobreak - Categoria">
                      <span itemprop="name">Nobreak - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';

$caminhotransformador='
                <div class="breadcrumb" id="breadcrumb">
                    <div class="wrapper">
                        <div class="bread__row">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a href="'.$url.'" itemprop="item" title="Home">
                        <span itemprop="name"> Home   </span>
                      </a>
                      <meta itemprop="position" content="1" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <a  href="'.$url.'produtos-categoria" itemprop="item" title="Produtos">
                        <span itemprop="name">Produtos   </span>
                      </a>
                      <meta itemprop="position" content="2" />
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                    <a  href="'.$url.'transformador-categoria" itemprop="item" title="Transformador - Categoria">
                      <span itemprop="name">Transformador - Categoria   </span>
                    </a>
                    <meta itemprop="position" content="3" />
                  </li>
                    <li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                      <span itemprop="name">'.$h1.'</span>
                      <meta itemprop="position" content="4" />
                    </li>
                  </ol>
                </nav>
                </div>
                </div>
                </div>
                ';
$caminhoinformacoes = '
<div class="breadcrumb" id="breadcrumb">
<div class="wrapper">
    <div class="bread__row">
<nav aria-label="breadcrumb">
<ol class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a href="'.$url.'" itemprop="item" title="Home">
    <span itemprop="name"> Home   </span>
  </a>
  <meta itemprop="position" content="1" />
</li>
<li class="breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <a  href="'.$url.'informacoes" itemprop="item" title="Informacoes">
    <span itemprop="name">Informacoes   </span>
  </a>
  <meta itemprop="position" content="2" />
</li>
<li class="breadcrumb-item active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
  <span itemprop="name">'.$h1.'</span>
  <meta itemprop="position" content="4" />
</li>
</ol>
</nav>
</div>
</div>
</div>
';

  