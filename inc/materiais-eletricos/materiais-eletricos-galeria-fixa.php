
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Tomada industrial','Tomada industrial 3 pinos','Tomada industrial 4 pinos','Tomada industrial 5 pinos','Tomada trifásica industrial','Tomadas externas industriais','Tomada industrial monofásica','Tomada 3 pinos industrial','Tomada tripolar industrial','Tomadas e plugues industriais','Plugues industriais','Plugues e tomadas industriais','Plug de tomada industrial','Tomada industrial de sobrepor','Unidades combinadas','Preço de unidades combinadas','Unidades combinadas tomadas','Unidade combinada strahl','Quadro de tomadas','Caixa de disjuntor com tomada','Quadro de tomadas para canteiro de obras','Quadro de tomadas para obra','Caixa de sobrepor com tomada e disjuntor','Quadro de tomadas industriais','Quadro de iluminação e tomadas','Caixa de medição de energia','Quadro de medição de energia','Caixa para medidor de energia trifásico','Caixa para medidor de energia monofásico','Caixa para medidor de energia preço','Caixa para medidor de energia bifásico','Caixa para medidor de energia elétrica','Preço caixa de medidor de energia','Caixa acrílica para medidor de energia preço','Centro de medição agrupada','Preço de centros de medição agrupada','Caixa de medição agrupada','Quadro de medidores','Quadro de medidores de energia','Quadro de medição elétrica','Quadro de medidores coletivo','Quadro para 6 medidores de energia','Quadro de medição agrupada','Quadro de medição trifásico','Quadro de luz para 4 medidores','Quadro de medição monofásico','Quadro de medição preço','Quadro de entrada de energia','Caixa de entrada de energia para 4 medidores','Caixa de entrada de energia trifásica','Caixa para entrada de energia elétrica','Quadro de distribuição de energia trifásico','Caixa de entrada de energia bifásico','Quadro de entrada de energia preço','Quadro de entrada de energia valor','Quadro de entrada de energia predial','Quadro de distribuição de energia montado','Quadro de energia de sobrepor','Caixa de passagem elétrica','Caixa de passagem 20x20','Caixa de passagem de concreto','Caixa de passagem 30x30','Caixa de passagem 15x15','Caixa de passagem eletroduto','Caixa de passagem de sobrepor','Caixa de passagem de embutir','Caixa de passagem externa','Caixa para string box','String box trifásico','String box trifásico para energia solar','Quadros elétricos','Quadro de comando elétrico','Quadro de energia','Fornecedores de quadros elétricos','Prensa cabo','Prensa cabo metálico','Prensa cabo aluminio','Prensa cabo elétrico','Prensa cabo pp','Prensa cabo bipartido','Prensa cabo para cabo chato','Prensa cabo 25mm','Prensa cabo preto','Caixa para hidrômetro','Caixa para 3 hidrômetro','Caixa para hidrometro concreto','Caixa de proteção para hidrômetro em policarbonato','Caixa para 2 hidrometros','Caixa para hidrômetro de água','Filtro de linha schuko','Filtro de linha tipo alemão','Filtro de linha profissional','Filtro de linha preço','Filtro de linha 8 tomadas','Filtro de linha 5 tomadas','Filtro de linha 3 tomadas','Filtro de linha 2 tomadas'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "materiais-eletricos";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/materiais-eletricos/materiais-eletricos-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/materiais-eletricos/materiais-eletricos-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/materiais-eletricos/thumbs/materiais-eletricos-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>