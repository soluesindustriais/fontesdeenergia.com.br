
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Apc nobreak','Assistência técnica nobreak','Bateria de nobreak','Bateria nobreak','Bateria para nobreak','Comprar nobreak','Comprar nobreak apc','Comprar nobreak sms','Empresas de nobreak em sp','Nobreak 1000va','Nobreak 1200va','Nobreak 1400va','Nobreak 1500va','Nobreak 2000va','Nobreak 2kva','Nobreak 3000va','Nobreak 3kva','Nobreak 600va','Nobreak 700va','Nobreak apc','Nobreak apc 1500va','Nobreak apc 600','Nobreak apc 600va','Nobreak apc assistência técnica','Nobreak eaton','Nobreak emerson','Nobreak industrial','Nobreak industrial preço','Nobreak liebert','Nobreak para máquinas industriais','Nobreak para pc','Nobreak preço','Nobreak senoidal','Nobreak senoidal puro','Nobreak sms','Nobreak sms 1200va','Nobreak sms 1400va','Nobreak sms 600va','Nobreak sms 700va','Preço de nobreak','Preço nobreak','Reparação de nobreak','Sms nobreak','Venda de nobreak'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "nobreak";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/nobreak/nobreak-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/nobreak/nobreak-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/nobreak/thumbs/nobreak-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>