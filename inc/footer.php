<footer id="rodape">
	<div class="wrapper">
		<div class="contact-footer">
			<address>
				<span><?=$nomeSite." - ".$slogan?></span>
			</address>
		</div>
		<div class="menu-footer">
			<nav>
				<ul>
					<li><a href="" title="Página inicial">Home</a></li>
					<li><a href="sobre-nos" title="Sobre nós">Sobre nós</a></li>
					<li><a href="informacoes" title="Informacoes">Informações</a></li>
					<li><a href="produtos-categoria" title="Produtos">Produtos</a></li>
					<li><a href="mapa-site" title="Mapa do site">Mapa do site</a></li>
				</ul>
			</nav>
		</div>
		<br class="clear">
	</div>
</footer>
<div class="copyright-footer" id="rodape2">
	<div class="wrapper-footer">
		Copyright © <?=$nomeSite?>. (Lei 9610 de 19/02/1998)
		<div class="center-footer">
			<img src="imagens/img-home/logo-footer.png" style="width:65px; height:65px; object-fit: contain;" alt="<?=$nomeSite?>" tile="<?=$nomeSite?>">
			<p>é um parceiro</p>
			<img src="imagens/logo-solucs.png" style="width:75px; height:75px; object-fit: contain;" alt="Soluções Industriais" title="Soluções Industriais">
		</div>
		<div class="selos">
			<a rel="noopener nofollow" href="http://validator.w3.org/check?uri=<?=$url.$urlPagina?>" target="_blank" title="HTML5 W3C"><i class="fa fa-html5"></i> <strong>W3C</strong></a>
			<a rel="noopener nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?=$url.$urlPagina?>" target="_blank" title="CSS W3C" ><i class="fa fa-css3"></i> <strong>W3C</strong></a>
		</div>
	</div>
</div>

<!-- Conteúdo do footer acima -->
<!-- Includes Detalhes -->
<?php include 'inc/fancy.php'; include 'inc/readmore.php' ?>
<script src="https://cdn.jsdelivr.net/gh/englishextra/iframe-lightbox@0.2.8/js/iframe-lightbox.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="https://cdn.jsdelivr.net/gh/englishextra/iframe-lightbox@0.2.8/css/iframe-lightbox.min.css">
<script>
	[].forEach.call(document.getElementsByClassName("iframe-lightbox-link"), function (el) {
		el.lightbox = new IframeLightbox(el);
	});


	(function (root, document) {
		"use strict";
		[].forEach.call(document.getElementsByClassName("iframe-lightbox-link"), function (el) {
			el.lightbox = new IframeLightbox(el, {
				onCreated: function () {
					/* show your preloader */
				},
				onLoaded: function () {
					/* hide your preloader */
				},
				onError: function () {
					/* hide your preloader */
				},
				onClosed: function () {
					/* hide your preloader */
				},
				scrolling: false,
				/* default: false */
				rate: 500 /* default: 500 */ ,
				touch: false /* default: false - use with care for responsive images in links on vertical mobile screens */
			});
		});
	})("transformador" !== typeof window ? window : this, document);
</script>
<!-- API botao-cotar -->
<script src="https://solucoesindustriais.com.br/js/dist/sdk-cotacao-solucs/package.js"></script>
<div id="sharkOrcamento" style="display: none;"></div>
<?php if($ter != 1): ?>
<script>
var guardar = document.querySelectorAll('.botao-cotar');
for(var i = 0; i < guardar.length; i++){
guardar[i].removeAttribute('href');
  var adicionando = guardar[i].parentNode;
  adicionando.classList.add('nova-api');
};
</script>
<?php endif; ?>
<script>
	let btnDeCotar = [...document.querySelectorAll('.botao-cotar')];
	btnDeCotar.map(cotar => {
		cotar.addEventListener('click', () => {
			valorCotar = cotar.getAttribute('title');
			console.log(valorCotar);
		});
	});
</script>
<!-- Script Aside-Launcher start -->

<!-- Outros Scripts  -->
<script>     
<?
include "js/geral.js";
include "js/scroll.js";
include "js/app.js";
include "js/click-actions.js";
?>
</script>
<!-- BOTAO SCROLL -->
<script async src="<?=$url?>js/jquery.scrollUp.min.js"></script>
<script>
	var resposta = document.querySelectorAll('img');
	// var  videoNo = document.querySelector('video-flex img').src;

	let lazyImages = [...document.querySelectorAll('.lazy')]
	let inAdvance = 300

	function lazyLoad() {
		lazyImages.forEach(image => {
			if (image.offsetTop < window.innerHeight + window.pageYOffset + inAdvance) {
				image.src = image.dataset.src
				image.onload = () => image.classList.add('loaded')
			}
		})
	}
	lazyLoad()
	document.addEventListener('scroll', function () {
		lazyLoad()
	});
</script>
<!-- TagManager  -->
<?php
foreach($tagsanalytic as $analytics){
    echo '<script async src="https://www.googletagmanager.com/gtag/js?id='.$analytics.'"></script>'.'<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);}'."gtag('js', new Date()); gtag('config', '$analytics')</script>";
}
?>

<!-- <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div> -->

<!-- <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script> -->
        <div style="display: none" id="exit-banner-div">
    <div data-sdk-ideallaunch="" data-placement="popup_exit" id="exit-banner-container"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.5/dist/js.cookie.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css" />

<script src='https://ideallaunch.solucoesindustriais.com.br/js/sdk/install.js'></script>

<script>
    setTimeout(function() {
        $("html").mouseleave(function() {
            if ($("#exit-banner-container").find('img').length > 0 &&
                !Cookies.get('banner_displayed')) {
                if ($('.fancybox-container').length == 0) {
                    $.fancybox.open({
                        src: '#exit-banner-div',
                        type: 'inline',
                        opts: {
                            afterShow: function() {
                                let minutesBanner = new Date(new Date().getTime() + 5 * 60 * 1000);
                                Cookies.set('banner_displayed', true, { expires: minutesBanner });
                            }
                        }
                    });
                }
            }
        });
    }, 4000);
</script>
<script type="module">
import Typebot from 'https://cdn.jsdelivr.net/npm/@typebot.io/js@0.2.81/dist/web.js'

Typebot.initBubble({
typebot: "chatbotsatelites",
prefilledVariables: {
source: 'Fontes de Energia',
URL: 'https://fontesdeenergia.com.br', },
apiHost: "https://chat.ferramentademarketing.com.br",
previewMessage: {
message:
"Oi! Posso te ajudar?",
autoShowDelay: 1000,
avatarUrl:
"https://s3.typebot.io/public/workspaces/clzir1det0001bsmim0a73co8/typebots/clzir2kub0005bsmicrxd9r3c/hostAvatar?v=1722968705385",
},
theme: {
button: { backgroundColor: "#003ac2" },
previewMessage: {
backgroundColor: "#0042DA",
textColor: "#FFFFFF",
closeButtonBackgroundColor: "#0042DA",
closeButtonIconColor: "#FFFFFF",
},
chatWindow: { backgroundColor: "#fff" },
},
});
</script>