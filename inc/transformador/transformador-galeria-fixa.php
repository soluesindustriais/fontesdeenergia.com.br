
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Análise de óleo de transformador','Análise de óleo isolante de transformadores','Assistência técnica de transformadores','Auto transformador 3000va','Auto transformador 5000va','Autotransformador','Comprar auto transformador','Conserto de transformador','Conversor 110 para 220','Conversor de 110 para 220','Conversor de 220 para 110','Empresa de transformadores a seco','Empresas fabricantes de transformadores de potência','Empresas fabricantes de transformadores elétricos','Fábrica de transformadores','Fábrica de transformadores a seco','Fabricante de autotransformadores','Fabricantes de transformadores de distribuição','Manutenção de transformadores','Manutenção de transformadores de potência','Manutenção em transformadores de distribuição','Preço transformador','Transformador 110 220','Transformador 110 para 220','Transformador 110 para 220 para ar condicionado','Transformador 110 para 220 preço','Transformador 110 para 220v','Transformador 110v para 220v','Transformador 220','Transformador 220 110','Transformador 220 para 110','Transformador 220v para 110v','Transformador 3000va','Transformador 5000va','Transformador a óleo','Transformador a seco','Transformador a venda','Transformador alta tensão','Transformador bivolt','Transformador de 110 para 220','Transformador de 220 para 110','Transformador de distribuição','Transformador de energia 110v para 220v','Transformador de energia 220 para 110','Transformador de força','Transformador de potencial','Transformador de voltagem','Transformador de voltagem 110 para 220','Transformador industrial','Transformador isolador trifásico','Transformador preço','Transformadores elétricos','Transformadores em sp','Transformadores especiais','Transformadores trifásicos','Venda de transformadores'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "transformador";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/transformador/transformador-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/transformador/transformador-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/transformador/thumbs/transformador-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>