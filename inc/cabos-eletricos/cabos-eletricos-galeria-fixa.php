
<link rel="stylesheet" href="<?= $url ?>css/thumbnails.css">
<?php
$galeriaItens = [];
?>
<?php $lista = array('Cabo 10mm preço','Cabo 16mm preço','Cabo 6mm preço','Cabo de alimentação','Cabo de alimentação para fonte','Cabo de alimentação pc','Cabo de energia','Cabo de energia notebook','Cabo de energia para fonte','Cabo de energia para notebook','Cabo de energia pc','Cabo de força','Cabo de força notebook','Cabo de força para cpu','Cabo de força para notebook','Cabo de força para pc','Cabo de força para tv','Cabo de força pc','Cabo de força tripolar','Cabo de força y para pc','Cabo de rede','Cabo de rede blindado','Cabo de rede preço','Cabo elétrico 16mm','Cabo flexivel 16mm','Cabo fonte notebook','Cabo injetado','Cabo pp com plug injetado','Cabos elétricos','Cabos elétricos preços','Empresa de cabo de força','Fábrica cabos injetados','Fábrica de cabos elétricos','Fabricante de cabos pp com plug','Fios e cabos elétricos','Fios e cabos elétricos preço','Preço de cabos elétricos'); shuffle($lista); for($i=1;$i<13;$i++){ ?>  
<?php
$folder = "cabos-eletricos";
$galeriaItens[] = [
    "@type" => "ImageObject",
    "author" => "Soluções Industriais",
    "contentUrl" => $url . "imagens/cabos-eletricos/cabos-eletricos-" . $i . ".jpg",
    "description" => $lista[$i],
    "name" => $lista[$i],
    "uploadDate" => "2024-02-20"
];
}
?>

<ul class="thumbnails-mod17">
<?php for ($i = 1; $i < 13; $i++) { ?>
    <li>
        <a class="lightbox" href="<?= $url; ?>imagens/cabos-eletricos/cabos-eletricos-<?= $i ?>.jpg" title="<?= $lista[$i] ?>">
            <img src="<?= $url; ?>imagens/cabos-eletricos/thumbs/cabos-eletricos-<?= $i ?>.jpg" class="lazyload" alt="<?= $lista[$i] ?>" title="<?= $lista[$i] ?>" />
        </a>
    </li>
<?php } ?>
</ul>

<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "ImageGallery",
    "mainEntity": {
        "@type": "ItemList",
        "itemListElement": <?php echo json_encode($galeriaItens); ?>
    },
    "name": "Modelo ilustrativo de <?= $h1 ?>",
    "description": "Imagem descritiva sobre <?= $h1 ?> afim de exemplificar sobre o produto."
}
</script>