<? $h1 = 'Informações';
$title = 'Informações';
$desc = 'Aluguel Empilhadeira - Saiba tudo sobre Empilhadeira. Faça cotações com diversas empresas de Empilhadeira gratuitamente';
include('inc/head.php'); ?></head>

<body><? include('inc/topo.php'); ?><div class="wrapper">
        <main>
            <div class="content">
                <section><?= $caminho2 ?><h1 class="titulo-informacoes"><?= $h1 ?></h1>
                    <article class="full">
                        <ul class="thumbnails-main">
                            <li><a href="<?= $url ?>auto-transformador" title="Auto transformador"><img src="imagens/mpi/thumbs/auto-transformador-01.jpg" alt="Auto transformador" title="Auto transformador" /></a>
                                <h2><a href="<?= $url ?>auto-transformador" title="Auto transformador">Auto transformador</a></h2>
                            </li>
                            <li><a href="<?= $url ?>auto-transformador-eletrico" title="Auto transformador elétrico"><img src="imagens/mpi/thumbs/auto-transformador-eletrico-01.jpg" alt="Auto transformador elétrico" title="Auto transformador elétrico" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-eletrico" title="Auto transformador elétrico">Auto transformador elétrico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>auto-transformador-trifasico" title="Auto transformador trifásico"><img src="imagens/mpi/thumbs/auto-transformador-trifasico-01.jpg" alt="Auto transformador trifásico" title="Auto transformador trifásico" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-trifasico" title="Auto transformador trifásico">Auto transformador trifásico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>compra-de-gerador-de-energia" title="Compra de gerador de energia"><img src="imagens/mpi/thumbs/compra-de-gerador-de-energia-01.jpg" alt="Compra de gerador de energia" title="Compra de gerador de energia" /></a>
                                <h2><a href="<?= $url ?>compra-de-gerador-de-energia" title="Compra de gerador de energia">Compra de gerador de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>compra-de-transformadores" title="Compra de transformadores"><img src="imagens/mpi/thumbs/compra-de-transformadores-01.jpg" alt="Compra de transformadores" title="Compra de transformadores" /></a>
                                <h2><a href="<?= $url ?>compra-de-transformadores" title="Compra de transformadores">Compra de transformadores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-gerador-de-energia" title="Comprar gerador de energia"><img src="imagens/mpi/thumbs/comprar-gerador-de-energia-01.jpg" alt="Comprar gerador de energia" title="Comprar gerador de energia" /></a>
                                <h2><a href="<?= $url ?>comprar-gerador-de-energia" title="Comprar gerador de energia">Comprar gerador de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-gerador-de-energia-a-diesel" title="Comprar gerador de energia a diesel"><img src="imagens/mpi/thumbs/comprar-gerador-de-energia-a-diesel-01.jpg" alt="Comprar gerador de energia a diesel" title="Comprar gerador de energia a diesel" /></a>
                                <h2><a href="<?= $url ?>comprar-gerador-de-energia-a-diesel" title="Comprar gerador de energia a diesel">Comprar gerador de energia a diesel</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-grupo-gerador" title="Comprar grupo gerador"><img src="imagens/mpi/thumbs/comprar-grupo-gerador-01.jpg" alt="Comprar grupo gerador" title="Comprar grupo gerador" /></a>
                                <h2><a href="<?= $url ?>comprar-grupo-gerador" title="Comprar grupo gerador">Comprar grupo gerador</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-placa-solar" title="Comprar placa solar"><img src="imagens/mpi/thumbs/comprar-placa-solar-01.jpg" alt="Comprar placa solar" title="Comprar placa solar" /></a>
                                <h2><a href="<?= $url ?>comprar-placa-solar" title="Comprar placa solar">Comprar placa solar</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-transformador" title="Comprar transformador"><img src="imagens/mpi/thumbs/comprar-transformador-01.jpg" alt="Comprar transformador" title="Comprar transformador" /></a>
                                <h2><a href="<?= $url ?>comprar-transformador" title="Comprar transformador">Comprar transformador</a></h2>
                            </li>
                            <li><a href="<?= $url ?>comprar-transformador-trifasico" title="Comprar transformador trifásico"><img src="imagens/mpi/thumbs/comprar-transformador-trifasico-01.jpg" alt="Comprar transformador trifásico" title="Comprar transformador trifásico" /></a>
                                <h2><a href="<?= $url ?>comprar-transformador-trifasico" title="Comprar transformador trifásico">Comprar transformador trifásico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>eficiencia-energetica" title="Eficiência energética"><img src="imagens/mpi/thumbs/eficiencia-energetica-01.jpg" alt="Eficiência energética" title="Eficiência energética" /></a>
                                <h2><a href="<?= $url ?>eficiencia-energetica" title="Eficiência energética">Eficiência energética</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-eficiencia-energetica" title="Empresa de eficiência energética"><img src="imagens/mpi/thumbs/empresa-de-eficiencia-energetica-01.jpg" alt="Empresa de eficiência energética" title="Empresa de eficiência energética" /></a>
                                <h2><a href="<?= $url ?>empresa-de-eficiencia-energetica" title="Empresa de eficiência energética">Empresa de eficiência energética</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-eletrica-industrial" title="Empresa de elétrica industrial"><img src="imagens/mpi/thumbs/empresa-de-eletrica-industrial-01.jpg" alt="Empresa de elétrica industrial" title="Empresa de elétrica industrial" /></a>
                                <h2><a href="<?= $url ?>empresa-de-eletrica-industrial" title="Empresa de elétrica industrial">Empresa de elétrica industrial</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-geradores" title="Empresa de geradores"><img src="imagens/mpi/thumbs/empresa-de-geradores-01.jpg" alt="Empresa de geradores" title="Empresa de geradores" /></a>
                                <h2><a href="<?= $url ?>empresa-de-geradores" title="Empresa de geradores">Empresa de geradores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-geradores-de-energia-sp" title="Empresa de geradores de energia sp"><img src="imagens/mpi/thumbs/empresa-de-geradores-de-energia-sp-01.jpg" alt="Empresa de geradores de energia sp" title="Empresa de geradores de energia sp" /></a>
                                <h2><a href="<?= $url ?>empresa-de-geradores-de-energia-sp" title="Empresa de geradores de energia sp">Empresa de geradores de energia sp</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-placas-solares" title="Empresa de placas solares"><img src="imagens/mpi/thumbs/empresa-de-placas-solares-01.jpg" alt="Empresa de placas solares" title="Empresa de placas solares" /></a>
                                <h2><a href="<?= $url ?>empresa-de-placas-solares" title="Empresa de placas solares">Empresa de placas solares</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-de-transformadores-eletricos" title="Empresa de transformadores elétricos"><img src="imagens/mpi/thumbs/empresa-de-transformadores-eletricos-01.jpg" alt="Empresa de transformadores elétricos" title="Empresa de transformadores elétricos" /></a>
                                <h2><a href="<?= $url ?>empresa-de-transformadores-eletricos" title="Empresa de transformadores elétricos">Empresa de transformadores elétricos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresa-fabricante-de-transformadores" title="Empresa fabricante de transformadores"><img src="imagens/mpi/thumbs/empresa-fabricante-de-transformadores-01.jpg" alt="Empresa fabricante de transformadores" title="Empresa fabricante de transformadores" /></a>
                                <h2><a href="<?= $url ?>empresa-fabricante-de-transformadores" title="Empresa fabricante de transformadores">Empresa fabricante de transformadores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresas-de-geradores-de-energia" title="Empresas de geradores de energia"><img src="imagens/mpi/thumbs/empresas-de-geradores-de-energia-01.jpg" alt="Empresas de geradores de energia" title="Empresas de geradores de energia" /></a>
                                <h2><a href="<?= $url ?>empresas-de-geradores-de-energia" title="Empresas de geradores de energia">Empresas de geradores de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresas-de-grupos-geradores" title="Empresas de grupos geradores"><img src="imagens/mpi/thumbs/empresas-de-grupos-geradores-01.jpg" alt="Empresas de grupos geradores" title="Empresas de grupos geradores" /></a>
                                <h2><a href="<?= $url ?>empresas-de-grupos-geradores" title="Empresas de grupos geradores">Empresas de grupos geradores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresas-de-transformadores" title="Empresas de transformadores"><img src="imagens/mpi/thumbs/empresas-de-transformadores-01.jpg" alt="Empresas de transformadores" title="Empresas de transformadores" /></a>
                                <h2><a href="<?= $url ?>empresas-de-transformadores" title="Empresas de transformadores">Empresas de transformadores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>empresas-de-transformadores-em-sp" title="Empresas de transformadores em sp"><img src="imagens/mpi/thumbs/empresas-de-transformadores-em-sp-01.jpg" alt="Empresas de transformadores em sp" title="Empresas de transformadores em sp" /></a>
                                <h2><a href="<?= $url ?>empresas-de-transformadores-em-sp" title="Empresas de transformadores em sp">Empresas de transformadores em sp</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabrica-de-transformadores-de-potencia" title="Fábrica de transformadores de potência"><img src="imagens/mpi/thumbs/fabrica-de-transformadores-de-potencia-01.jpg" alt="Fábrica de transformadores de potência" title="Fábrica de transformadores de potência" /></a>
                                <h2><a href="<?= $url ?>fabrica-de-transformadores-de-potencia" title="Fábrica de transformadores de potência">Fábrica de transformadores de potência</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabrica-de-transformadores-eletricos" title="Fábrica de transformadores elétricos"><img src="imagens/mpi/thumbs/fabrica-de-transformadores-eletricos-01.jpg" alt="Fábrica de transformadores elétricos" title="Fábrica de transformadores elétricos" /></a>
                                <h2><a href="<?= $url ?>fabrica-de-transformadores-eletricos" title="Fábrica de transformadores elétricos">Fábrica de transformadores elétricos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabrica-de-transformadores-trifasicos" title="Fábrica de transformadores trifásicos"><img src="imagens/mpi/thumbs/fabrica-de-transformadores-trifasicos-01.jpg" alt="Fábrica de transformadores trifásicos" title="Fábrica de transformadores trifásicos" /></a>
                                <h2><a href="<?= $url ?>fabrica-de-transformadores-trifasicos" title="Fábrica de transformadores trifásicos">Fábrica de transformadores trifásicos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricante-de-transformadores-a-seco" title="Fabricante de transformadores a seco"><img src="imagens/mpi/thumbs/fabricante-de-transformadores-a-seco-01.jpg" alt="Fabricante de transformadores a seco" title="Fabricante de transformadores a seco" /></a>
                                <h2><a href="<?= $url ?>fabricante-de-transformadores-a-seco" title="Fabricante de transformadores a seco">Fabricante de transformadores a seco</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricantes-de-transformadores" title="Fabricantes de transformadores"><img src="imagens/mpi/thumbs/fabricantes-de-transformadores-01.jpg" alt="Fabricantes de transformadores" title="Fabricantes de transformadores" /></a>
                                <h2><a href="<?= $url ?>fabricantes-de-transformadores" title="Fabricantes de transformadores">Fabricantes de transformadores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricantes-de-transformadores-de-potencia" title="Fabricantes de transformadores de potência"><img src="imagens/mpi/thumbs/fabricantes-de-transformadores-de-potencia-01.jpg" alt="Fabricantes de transformadores de potência" title="Fabricantes de transformadores de potência" /></a>
                                <h2><a href="<?= $url ?>fabricantes-de-transformadores-de-potencia" title="Fabricantes de transformadores de potência">Fabricantes de transformadores de potência</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricantes-de-transformadores-eletricos" title="Fabricantes de transformadores elétricos"><img src="imagens/mpi/thumbs/fabricantes-de-transformadores-eletricos-01.jpg" alt="Fabricantes de transformadores elétricos" title="Fabricantes de transformadores elétricos" /></a>
                                <h2><a href="<?= $url ?>fabricantes-de-transformadores-eletricos" title="Fabricantes de transformadores elétricos">Fabricantes de transformadores elétricos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricantes-de-transformadores-tipo-seco" title="Fabricantes de transformadores tipo seco"><img src="imagens/mpi/thumbs/fabricantes-de-transformadores-tipo-seco-01.jpg" alt="Fabricantes de transformadores tipo seco" title="Fabricantes de transformadores tipo seco" /></a>
                                <h2><a href="<?= $url ?>fabricantes-de-transformadores-tipo-seco" title="Fabricantes de transformadores tipo seco">Fabricantes de transformadores tipo seco</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fabricantes-de-transformadores-trifasicos" title="Fabricantes de transformadores trifásicos"><img src="imagens/mpi/thumbs/fabricantes-de-transformadores-trifasicos-01.jpg" alt="Fabricantes de transformadores trifásicos" title="Fabricantes de transformadores trifásicos" /></a>
                                <h2><a href="<?= $url ?>fabricantes-de-transformadores-trifasicos" title="Fabricantes de transformadores trifásicos">Fabricantes de transformadores trifásicos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-de-alimentacao-ac" title="Fonte de alimentação ac"><img src="imagens/mpi/thumbs/fonte-de-alimentacao-ac-01.jpg" alt="Fonte de alimentação ac" title="Fonte de alimentação ac" /></a>
                                <h2><a href="<?= $url ?>fonte-de-alimentacao-ac" title="Fonte de alimentação ac">Fonte de alimentação ac</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-de-alimentacao-dc" title="Fonte de alimentação dc"><img src="imagens/mpi/thumbs/fonte-de-alimentacao-dc-01.jpg" alt="Fonte de alimentação dc" title="Fonte de alimentação dc" /></a>
                                <h2><a href="<?= $url ?>fonte-de-alimentacao-dc" title="Fonte de alimentação dc">Fonte de alimentação dc</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-de-alimentacao-programavel" title="Fonte de alimentação programável"><img src="imagens/mpi/thumbs/fonte-de-alimentacao-programavel-01.jpg" alt="Fonte de alimentação programável" title="Fonte de alimentação programável" /></a>
                                <h2><a href="<?= $url ?>fonte-de-alimentacao-programavel" title="Fonte de alimentação programável">Fonte de alimentação programável</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-de-alimentacao-variavel" title="Fonte de alimentação variável"><img src="imagens/mpi/thumbs/fonte-de-alimentacao-variavel-01.jpg" alt="Fonte de alimentação variável" title="Fonte de alimentação variável" /></a>
                                <h2><a href="<?= $url ?>fonte-de-alimentacao-variavel" title="Fonte de alimentação variável">Fonte de alimentação variável</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-de-quatro-quadrantes" title="Fonte de quatro quadrantes"><img src="imagens/mpi/thumbs/fonte-de-quatro-quadrantes-01.jpg" alt="Fonte de quatro quadrantes" title="Fonte de quatro quadrantes" /></a>
                                <h2><a href="<?= $url ?>fonte-de-quatro-quadrantes" title="Fonte de quatro quadrantes">Fonte de quatro quadrantes</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fonte-regenerativa" title="Fonte regenerativa"><img src="imagens/mpi/thumbs/fonte-regenerativa-01.jpg" alt="Fonte regenerativa" title="Fonte regenerativa" /></a>
                                <h2><a href="<?= $url ?>fonte-regenerativa" title="Fonte regenerativa">Fonte regenerativa</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fornecedor-de-transformador-de-potencia" title="Fornecedor de transformador de potência"><img src="imagens/mpi/thumbs/fornecedor-de-transformador-de-potencia-01.jpg" alt="Fornecedor de transformador de potência" title="Fornecedor de transformador de potência" /></a>
                                <h2><a href="<?= $url ?>fornecedor-de-transformador-de-potencia" title="Fornecedor de transformador de potência">Fornecedor de transformador de potência</a></h2>
                            </li>
                            <li><a href="<?= $url ?>fornecedores-de-transformadores" title="Fornecedores de transformadores"><img src="imagens/mpi/thumbs/fornecedores-de-transformadores-01.jpg" alt="Fornecedores de transformadores" title="Fornecedores de transformadores" /></a>
                                <h2><a href="<?= $url ?>fornecedores-de-transformadores" title="Fornecedores de transformadores">Fornecedores de transformadores</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-a-diesel" title="Gerador de energia a diesel"><img src="imagens/mpi/thumbs/gerador-de-energia-a-diesel-01.jpg" alt="Gerador de energia a diesel" title="Gerador de energia a diesel" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-a-diesel" title="Gerador de energia a diesel">Gerador de energia a diesel</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-a-diesel-pequeno" title="Gerador de energia a diesel pequeno"><img src="imagens/mpi/thumbs/gerador-de-energia-a-diesel-pequeno-01.jpg" alt="Gerador de energia a diesel pequeno" title="Gerador de energia a diesel pequeno" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-a-diesel-pequeno" title="Gerador de energia a diesel pequeno">Gerador de energia a diesel pequeno</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-a-diesel-silencioso" title="Gerador de energia a diesel silencioso"><img src="imagens/mpi/thumbs/gerador-de-energia-a-diesel-silencioso-01.jpg" alt="Gerador de energia a diesel silencioso" title="Gerador de energia a diesel silencioso" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-a-diesel-silencioso" title="Gerador de energia a diesel silencioso">Gerador de energia a diesel silencioso</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-a-diesel-trifasico" title="Gerador de energia a diesel trifásico"><img src="imagens/mpi/thumbs/gerador-de-energia-a-diesel-trifasico-01.jpg" alt="Gerador de energia a diesel trifásico" title="Gerador de energia a diesel trifásico" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-a-diesel-trifasico" title="Gerador de energia a diesel trifásico">Gerador de energia a diesel trifásico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-para-condominio" title="Gerador de energia para condomínio"><img src="imagens/mpi/thumbs/gerador-de-energia-para-condominio-01.jpg" alt="Gerador de energia para condomínio" title="Gerador de energia para condomínio" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-para-condominio" title="Gerador de energia para condomínio">Gerador de energia para condomínio</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-de-energia-para-empresa" title="Gerador de energia para empresa"><img src="imagens/mpi/thumbs/gerador-de-energia-para-empresa-01.jpg" alt="Gerador de energia para empresa" title="Gerador de energia para empresa" /></a>
                                <h2><a href="<?= $url ?>gerador-de-energia-para-empresa" title="Gerador de energia para empresa">Gerador de energia para empresa</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-para-empresa" title="Gerador para empresa"><img src="imagens/mpi/thumbs/gerador-para-empresa-01.jpg" alt="Gerador para empresa" title="Gerador para empresa" /></a>
                                <h2><a href="<?= $url ?>gerador-para-empresa" title="Gerador para empresa">Gerador para empresa</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-para-eventos" title="Gerador para eventos"><img src="imagens/mpi/thumbs/gerador-para-eventos-01.jpg" alt="Gerador para eventos" title="Gerador para eventos" /></a>
                                <h2><a href="<?= $url ?>gerador-para-eventos" title="Gerador para eventos">Gerador para eventos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>gerador-para-festa" title="Gerador para festa"><img src="imagens/mpi/thumbs/gerador-para-festa-01.jpg" alt="Gerador para festa" title="Gerador para festa" /></a>
                                <h2><a href="<?= $url ?>gerador-para-festa" title="Gerador para festa">Gerador para festa</a></h2>
                            </li>
                            <li><a href="<?= $url ?>geradores-de-energia-em-sp" title="Geradores de energia em sp"><img src="imagens/mpi/thumbs/geradores-de-energia-em-sp-01.jpg" alt="Geradores de energia em sp" title="Geradores de energia em sp" /></a>
                                <h2><a href="<?= $url ?>geradores-de-energia-em-sp" title="Geradores de energia em sp">Geradores de energia em sp</a></h2>
                            </li>
                            <li><a href="<?= $url ?>geradores-para-industrias" title="Geradores para indústrias"><img src="imagens/mpi/thumbs/geradores-para-industrias-01.jpg" alt="Geradores para indústrias" title="Geradores para indústrias" /></a>
                                <h2><a href="<?= $url ?>geradores-para-industrias" title="Geradores para indústrias">Geradores para indústrias</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-a-diesel-preco" title="Grupo gerador a diesel preço"><img src="imagens/mpi/thumbs/grupo-gerador-a-diesel-preco-01.jpg" alt="Grupo gerador a diesel preço" title="Grupo gerador a diesel preço" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-a-diesel-preco" title="Grupo gerador a diesel preço">Grupo gerador a diesel preço</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-carenado" title="Grupo gerador carenado"><img src="imagens/mpi/thumbs/grupo-gerador-carenado-01.jpg" alt="Grupo gerador carenado" title="Grupo gerador carenado" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-carenado" title="Grupo gerador carenado">Grupo gerador carenado</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-de-energia" title="Grupo gerador de energia"><img src="imagens/mpi/thumbs/grupo-gerador-de-energia-01.jpg" alt="Grupo gerador de energia" title="Grupo gerador de energia" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-de-energia" title="Grupo gerador de energia">Grupo gerador de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-de-energia-a-venda" title="Grupo gerador de energia a venda"><img src="imagens/mpi/thumbs/grupo-gerador-de-energia-a-venda-01.jpg" alt="Grupo gerador de energia a venda" title="Grupo gerador de energia a venda" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-de-energia-a-venda" title="Grupo gerador de energia a venda">Grupo gerador de energia a venda</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-de-energia-eletrica" title="Grupo gerador de energia elétrica"><img src="imagens/mpi/thumbs/grupo-gerador-de-energia-eletrica-01.jpg" alt="Grupo gerador de energia elétrica" title="Grupo gerador de energia elétrica" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-de-energia-eletrica" title="Grupo gerador de energia elétrica">Grupo gerador de energia elétrica</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-de-energia-eletrica-a-diesel" title="Grupo gerador de energia elétrica a diesel"><img src="imagens/mpi/thumbs/grupo-gerador-de-energia-eletrica-a-diesel-01.jpg" alt="Grupo gerador de energia elétrica a diesel" title="Grupo gerador de energia elétrica a diesel" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-de-energia-eletrica-a-diesel" title="Grupo gerador de energia elétrica a diesel">Grupo gerador de energia elétrica a diesel</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-eletrico" title="Grupo gerador elétrico"><img src="imagens/mpi/thumbs/grupo-gerador-eletrico-01.jpg" alt="Grupo gerador elétrico" title="Grupo gerador elétrico" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-eletrico" title="Grupo gerador elétrico">Grupo gerador elétrico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupo-gerador-novo" title="Grupo gerador novo"><img src="imagens/mpi/thumbs/grupo-gerador-novo-01.jpg" alt="Grupo gerador novo" title="Grupo gerador novo" /></a>
                                <h2><a href="<?= $url ?>grupo-gerador-novo" title="Grupo gerador novo">Grupo gerador novo</a></h2>
                            </li>
                            <li><a href="<?= $url ?>grupos-geradores-a-diesel" title="Grupos geradores a diesel"><img src="imagens/mpi/thumbs/grupos-geradores-a-diesel-01.jpg" alt="Grupos geradores a diesel" title="Grupos geradores a diesel" /></a>
                                <h2><a href="<?= $url ?>grupos-geradores-a-diesel" title="Grupos geradores a diesel">Grupos geradores a diesel</a></h2>
                            </li>
                            <li><a href="<?= $url ?>onde-comprar-placa-solar" title="Onde comprar placa solar"><img src="imagens/mpi/thumbs/onde-comprar-placa-solar-01.jpg" alt="Onde comprar placa solar" title="Onde comprar placa solar" /></a>
                                <h2><a href="<?= $url ?>onde-comprar-placa-solar" title="Onde comprar placa solar">Onde comprar placa solar</a></h2>
                            </li>
                            <li><a href="<?= $url ?>onde-comprar-transformador" title="Onde comprar transformador"><img src="imagens/mpi/thumbs/onde-comprar-transformador-01.jpg" alt="Onde comprar transformador" title="Onde comprar transformador" /></a>
                                <h2><a href="<?= $url ?>onde-comprar-transformador" title="Onde comprar transformador">Onde comprar transformador</a></h2>
                            </li>
                            <li><a href="<?= $url ?>onde-comprar-transformador-de-voltagem" title="Onde comprar transformador de voltagem"><img src="imagens/mpi/thumbs/onde-comprar-transformador-de-voltagem-01.jpg" alt="Onde comprar transformador de voltagem" title="Onde comprar transformador de voltagem" /></a>
                                <h2><a href="<?= $url ?>onde-comprar-transformador-de-voltagem" title="Onde comprar transformador de voltagem">Onde comprar transformador de voltagem</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-de-energia-solar" title="Placa de energia solar"><img src="imagens/mpi/thumbs/placa-de-energia-solar-01.jpg" alt="Placa de energia solar" title="Placa de energia solar" /></a>
                                <h2><a href="<?= $url ?>placa-de-energia-solar" title="Placa de energia solar">Placa de energia solar</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-de-energia-solar-industrial" title="Placa de energia solar industrial"><img src="imagens/mpi/thumbs/placa-de-energia-solar-industrial-01.jpg" alt="Placa de energia solar industrial" title="Placa de energia solar industrial" /></a>
                                <h2><a href="<?= $url ?>placa-de-energia-solar-industrial" title="Placa de energia solar industrial">Placa de energia solar industrial</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-de-energia-solar-preco" title="Placa de energia solar preço"><img src="imagens/mpi/thumbs/placa-de-energia-solar-preco-01.jpg" alt="Placa de energia solar preço" title="Placa de energia solar preço" /></a>
                                <h2><a href="<?= $url ?>placa-de-energia-solar-preco" title="Placa de energia solar preço">Placa de energia solar preço</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-de-energia-solar-valor" title="Placa de energia solar valor"><img src="imagens/mpi/thumbs/placa-de-energia-solar-valor-01.jpg" alt="Placa de energia solar valor" title="Placa de energia solar valor" /></a>
                                <h2><a href="<?= $url ?>placa-de-energia-solar-valor" title="Placa de energia solar valor">Placa de energia solar valor</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-solar-industrial" title="Placa solar industrial"><img src="imagens/mpi/thumbs/placa-solar-industrial-01.jpg" alt="Placa solar industrial" title="Placa solar industrial" /></a>
                                <h2><a href="<?= $url ?>placa-solar-industrial" title="Placa solar industrial">Placa solar industrial</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-solar-preco" title="Placa solar preço"><img src="imagens/mpi/thumbs/placa-solar-preco-01.jpg" alt="Placa solar preço" title="Placa solar preço" /></a>
                                <h2><a href="<?= $url ?>placa-solar-preco" title="Placa solar preço">Placa solar preço</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-solar-quanto-custa" title="Placa solar quanto custa"><img src="imagens/mpi/thumbs/placa-solar-quanto-custa-01.jpg" alt="Placa solar quanto custa" title="Placa solar quanto custa" /></a>
                                <h2><a href="<?= $url ?>placa-solar-quanto-custa" title="Placa solar quanto custa">Placa solar quanto custa</a></h2>
                            </li>
                            <li><a href="<?= $url ?>placa-solar-venda" title="Placa solar venda"><img src="imagens/mpi/thumbs/placa-solar-venda-01.jpg" alt="Placa solar venda" title="Placa solar venda" /></a>
                                <h2><a href="<?= $url ?>placa-solar-venda" title="Placa solar venda">Placa solar venda</a></h2>
                            </li>
                            <li><a href="<?= $url ?>preco-de-placa-solar" title="Preço de placa solar"><img src="imagens/mpi/thumbs/preco-de-placa-solar-01.jpg" alt="Preço de placa solar" title="Preço de placa solar" /></a>
                                <h2><a href="<?= $url ?>preco-de-placa-solar" title="Preço de placa solar">Preço de placa solar</a></h2>
                            </li>
                            <li><a href="<?= $url ?>preco-de-transformador" title="Preço de transformador"><img src="imagens/mpi/thumbs/preco-de-transformador-01.jpg" alt="Preço de transformador" title="Preço de transformador" /></a>
                                <h2><a href="<?= $url ?>preco-de-transformador" title="Preço de transformador">Preço de transformador</a></h2>
                            </li>
                            <li><a href="<?= $url ?>preco-de-transformador-trifasico" title="Preço de transformador trifásico"><img src="imagens/mpi/thumbs/preco-de-transformador-trifasico-01.jpg" alt="Preço de transformador trifásico" title="Preço de transformador trifásico" /></a>
                                <h2><a href="<?= $url ?>preco-de-transformador-trifasico" title="Preço de transformador trifásico">Preço de transformador trifásico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>preco-de-transformadores-eletricos" title="Preço de transformadores elétricos"><img src="imagens/mpi/thumbs/preco-de-transformadores-eletricos-01.jpg" alt="Preço de transformadores elétricos" title="Preço de transformadores elétricos" /></a>
                                <h2><a href="<?= $url ?>preco-de-transformadores-eletricos" title="Preço de transformadores elétricos">Preço de transformadores elétricos</a></h2>
                            </li>
                            <li><a href="<?= $url ?>preco-do-transformador-de-energia" title="Preço do transformador de energia"><img src="imagens/mpi/thumbs/preco-do-transformador-de-energia-01.jpg" alt="Preço do transformador de energia" title="Preço do transformador de energia" /></a>
                                <h2><a href="<?= $url ?>preco-do-transformador-de-energia" title="Preço do transformador de energia">Preço do transformador de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>transformador-de-distribuicao-trifasico" title="Transformador de distribuição trifásico"><img src="imagens/mpi/thumbs/transformador-de-distribuicao-trifasico-01.jpg" alt="Transformador de distribuição trifásico" title="Transformador de distribuição trifásico" /></a>
                                <h2><a href="<?= $url ?>transformador-de-distribuicao-trifasico" title="Transformador de distribuição trifásico">Transformador de distribuição trifásico</a></h2>
                            </li>
                            <li><a href="<?= $url ?>transformador-de-energia" title="Transformador de energia"><img src="imagens/mpi/thumbs/transformador-de-energia-01.jpg" alt="Transformador de energia" title="Transformador de energia" /></a>
                                <h2><a href="<?= $url ?>transformador-de-energia" title="Transformador de energia">Transformador de energia</a></h2>
                            </li>
                            <li><a href="<?= $url ?>transformador-de-energia-preco" title="Transformador de energia preço"><img src="imagens/mpi/thumbs/transformador-de-energia-preco-01.jpg" alt="Transformador de energia preço" title="Transformador de energia preço" /></a>
                                <h2><a href="<?= $url ?>transformador-de-energia-preco" title="Transformador de energia preço">Transformador de energia preço</a></h2>
                            </li>
                            <li><a href="<?= $url ?>transformador-de-energia-trifasico" title="Transformador de energia trifásico"><img src="imagens/mpi/thumbs/transformador-de-energia-trifasico-01.jpg" alt="Transformador de energia trifásico" title="Transformador de energia trifásico" /></a>
                                <h2><a href="<?= $url ?>transformador-de-energia-trifasico" title="Transformador de energia trifásico">Transformador de energia trifásico</a></h2>
                            </li>


                            <!-- guilherme 05/01/23 -->
                            <li><a href="<?= $url ?>auto-transformador-a-oleo" title="Auto transformador a oleo"><img src="imagens/mpi/thumbs/auto-transformador-a-oleo-01.jpg" alt="Auto transformador a oleo" title="Auto transformador a oleo" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-a-oleo" title="Auto transformador a oleo">Auto transformador a oleo</a></h2>
                            </li>
                            <li><a href="<?= $url ?>auto-transformador-a-seco-ip21" title="Auto transformador a seco IP21"><img src="imagens/mpi/thumbs/auto-transformador-a-seco-ip21-01.jpg" alt="Auto transformador a seco IP21" title="Auto transformador a seco IP21" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-a-seco-ip21" title="Auto transformador a seco IP21">Auto transformador a seco IP21</a></h2>
                            </li>
                            <li><a href="<?= $url ?>auto-transformador-a-seco-ip23" title="Auto transformador a seco IP23"><img src="imagens/mpi/thumbs/auto-transformador-a-seco-ip23-01.jpg" alt="Auto transformador a seco IP23" title="Auto transformador a seco IP23" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-a-seco-ip23" title="Auto transformador a seco IP23">Auto transformador a seco IP23</a></h2>
                            </li>
                            <li><a href="<?= $url ?>auto-transformador-a-seco-ip54" title="Auto transformador a seco IP54"><img src="imagens/mpi/thumbs/auto-transformador-a-seco-ip54-01.jpg" alt="Auto transformador a seco IP54" title="Auto transformador a seco IP54" /></a>
                                <h2><a href="<?= $url ?>auto-transformador-a-seco-ip54" title="Auto transformador a seco IP54">Auto transformador a seco IP54</a></h2>
                            </li>


                        </ul>
                    </article>
                </section>
            </div>
        </main>
    </div><? include('inc/footer.php'); ?></body>

</html>